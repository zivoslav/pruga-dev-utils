#!/usr/bin/env sh

echo "--- ПРУГА >> RUN LIVERELOAD ---"
mate-terminal --tab --title "LIVERELOAD" --command "./bin/livereload-server.sh"

echo "--- ПРУГА >> RUN WWW SERVER ---"
# cd ./build/dev
mate-terminal --tab --title "HTTP SERVER" --command "./bin/server.sh"

echo "--- ПРУГА >> RUN WATCHING ---"
mate-terminal --tab --title "WATCHING" --command "./bin/watching.sh"


echo "--- ПРУГА >> RUN EDITOR ---"
code .
