#!/usr/bin/env fish

set PROJECT_DIRECTORY (dirname (status -f))

# -------------- SET LIVERELOAD ---------------

set LIVERELOAD_PORT 35729
set LIVERELOAD "$VIRTUAL_ENV/bin/livereload-server.js  $LIVERELOAD_PORT"



# -------------- SET SERVER ---------------

set SERVER_PORT 8081
set SERVER_ROOT_DIRECTORY $PROJECT_DIRECTORY/dev/book

# set SERVER "python -m http.server $SERVER_PORT"
# python --version
# python -m http.server 8081

set SERVER "php -S localhost:$SERVER_PORT"

# php --version
# php -S localhost:8081

# -------------- SET WATCHING ---------------

set WATCH "chokidar $SERVER_ROOT_DIRECTORY --command 'http GET http://localhost:$LIVERELOAD_PORT/changed\?files={path}'"
# -------------- RUN ---------------
echo "--- ПРУГА >> RUN EDITOR ---"
code .

echo "--- ПРУГА >> RUN LIVERELOAD ---"
echo $LIVERELOAD
mate-terminal --tab --title "LIVERELOAD" --command "$LIVERELOAD"

echo "--- ПРУГА >> RUN WATCHING ---"
echo $WATCH
mate-terminal --tab --title "WATCHING" --command "$WATCH"

echo "--- ПРУГА >> RUN WWW SERVER ---"
# echo $SERVER_ROOT_DIRECTORY
# !!!! je problém při změně adresáře, když je tento příkaz až poslední, tak to funguje
cd $SERVER_ROOT_DIRECTORY
echo $SERVER
mate-terminal --tab --title "HTTP SERVER" --command "$SERVER"


